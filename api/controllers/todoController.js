let mockTasks = require('../data/mockTasks');

exports.list_all_tasks = function(req, res) {
    try {
    const csrf = req.csrf;
    const xGatewayApiKey = req.xGatewayApiKey;
    res.status(200).send( {
        "csrf-token" :csrf,
        "x-Gateway-ApiKey": xGatewayApiKey,
        "tasks": mockTasks });
    } catch (error){
        res.status(500).send( {"error" : "error fetching data"} );
    }
};

exports.create_a_task = function(req, res) {
    const csrf = req.csrf;
    const xGatewayApiKey = req.xGatewayApiKey;
    const taskId =  req.body.id ;
    const description =  req.body.description ;
    if (description && taskId) {
        const newTask  = {
            id : taskId ,
            description: req.body.description
        }
        mockTasks= mockTasks.concat(newTask)
        res.status(200).send( {
            "csrf-token" :csrf,
            "x-Gateway-ApiKey": xGatewayApiKey,
            "tasks": newTask });
    } else {
        res.status(500).send( {
            "error" : "bad request to create task"
        });
    }
};

exports.read_a_task = function(req, res) {
    const csrf = req.csrf;
    const xGatewayApiKey = req.xGatewayApiKey;
    const taskId = req.params.taskId ;
    const foundTask= mockTasks.find(x => x.id === taskId);
    if(foundTask){
    res.status(200).send( {
        "csrf-token" :csrf,
        "x-Gateway-ApiKey": xGatewayApiKey,
        "tasks": foundTask });
    } else {
        res.status(404).send( {
            "message" :"task not found"
        });
    }
    };
exports.update_a_task = function(req, res) {
    const csrf = req.csrf;
    const xGatewayApiKey = req.xGatewayApiKey;
    const taskId =  req.body.id ;
    const description =  req.body.description ;
    if (description && taskId) {
        const updatedTask  = {
            id : taskId , 
            description: req.body.description
        }
        mockTasks= mockTasks.filter(item => item.id !== taskId);
        mockTasks= mockTasks.concat(updatedTask)
        res.status(200).send( {
            "csrf-token" :csrf,
            "x-Gateway-ApiKey": xGatewayApiKey,
            "updatedTask": updatedTask});
    } else {
        res.status(500).send( {
            "error" : "bad request to create task"
        });
    }

};

exports.delete_a_task = function(req, res) {
    try {
    const csrf = req.csrf;
    const xGatewayApiKey = req.xGatewayApiKey;
    const taskId = req.params.taskId;
    mockTasks = mockTasks.filter(item => item.id !== taskId);
    res.status(204).send( {
        "csrf-token" :csrf,
        "x-Gateway-ApiKey": xGatewayApiKey,
        "tasks": mockTasks});
    }catch (error) {
        res.status(404).send( {
            "error": "error while deleting task"});
    }
};