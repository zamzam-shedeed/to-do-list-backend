module.exports = function(app) {
    const todoList = require('../controllers/todoController');
    const responseInterceptor = require('../middlewares/response-interceptor');

    // Add new task
    app.post('/todo/App/api/todo',responseInterceptor,todoList.create_a_task);

    // Retrieve all tasks
    app.get('/todo/App/api/todos',responseInterceptor, todoList.list_all_tasks);

    // Retrieve a single task with taskId
    app.get('/todo/App/api/todo/:taskId',responseInterceptor, todoList.read_a_task);

    // Update a task with taskID
    app.put('/todo/App/api/todo/:taskId',responseInterceptor, todoList.update_a_task);

    // Delete a task with taskId
    app.delete('/todo/App/api/todo/:taskId', responseInterceptor,todoList.delete_a_task);
  };
  
  