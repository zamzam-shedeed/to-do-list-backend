fs = require('fs');
module.exports = (req , res , next ) => {
    try{
       var time = new Date()
       var method = req.method;
       var baseUrl = req.originalUrl;
       var version = 'HTTP/' + req.httpVersion;
       var status = res.statusCode;
        const writetofile = JSON.stringify({
            ipAddress : req.ip,
            time,
            method,
            baseUrl,
            version,
            status
        })
        const appendNewLine =',\n'
        fs.appendFile("log.json", writetofile+appendNewLine , (err) => {
          if(err) {
            console.log("Error with logging to file" , err);
          }
        });
      next();
    }catch(err){
      console.log(err);
      res.status(401).json({message : "logging to log.json file failed"});
    }
  }
