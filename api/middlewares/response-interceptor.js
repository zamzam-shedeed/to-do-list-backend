module.exports = (req , res , next ) => {
    try{
      req.csrf = Math.random() * (100 - 1) + 1;
      req.xGatewayApiKey = "Fixed-Api-key";
      next();
    }catch(err){
      console.log(err);
      res.status(401).json({message : "Failed to add tokens to response"});
    }
  }
