const request = require('supertest')
const app = require('../../server')
describe('task Endpoints', () => {
  it('should create a new task', async () => {
    const res = await request(app)
      .post('/todo/App/api/todo')
      .send({
        id : "1" ,
        description: "startttttt playing"
      })
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('tasks')
  })

it('should fetch a single task', async () => {
    const taskId = 1;
    const res = await request(app).get(`/todo/App/api/todo/${taskId}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('tasks');
  });

  it('should fetch all tasks', async () => {
    const res = await request(app).get('/todo/App/api/todos');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('tasks');
  });

  it('should update a task', async () => {
    const res = await request(app)
      .put('/todo/App/api/todo/1')
      .send({
        id : "1" ,
        description: "stop playing"
      });

    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('updatedTask');
    expect(res.body.updatedTask).toHaveProperty('description', 'stop playing');
    expect(res.body.updatedTask).toHaveProperty('id', '1');
  });

  it('should delete a task', async () => {
    const res = await request(app).delete('/todo/App/api/todo/1');
    expect(res.statusCode).toEqual(204);
  });

  it('should respond with status code 404 if task is not found', async () => {
    const taskId = 4;
    const res = await request(app).get(`/todo/App/api/todo/${taskId}`);
    expect(res.statusCode).toEqual(404);
  });

  it('should return status code 500 if create new task constraint is violated', async () => {
    const res = await request(app)
      .post('/todo/App/api/todo')
      .send({
        id: '1'
        });
    expect(res.statusCode).toEqual(500);
    expect(res.body).toHaveProperty('error');
  });

  it('should return status code 500 if update task constraint is violated', async () => {
    const res = await request(app)
      .post('/todo/App/api/todo')
      .send({
        id: '1'
        });
    expect(res.statusCode).toEqual(500);
    expect(res.body).toHaveProperty('error');
  });
})

